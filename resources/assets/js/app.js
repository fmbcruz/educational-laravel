
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('form-head', require('./components/Form.vue'));
Vue.component('editor', require('./components/Editor.vue'));
Vue.component('page', require('./components/Page.vue'));
Vue.component('card-text', require('./components/CardText.vue'));
Vue.component('alert', require('./components/Alert.vue'));
Vue.component('box', require('./components/Box.vue'));
Vue.component('table-list', require('./components/Table.vue'));
Vue.component('panel', require('./components/Panel.vue'));
Vue.component('breadcrumbs', require('./components/Breadcrumb.vue'));
Vue.component('alternatives', require('./components/Alternatives.vue'));

const app = new Vue({
    el: '#app',
    mounted: function() {
        $("#app").show();
    }
});
