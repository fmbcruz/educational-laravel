@extends('layouts.app')

@section('content')
    <page size="12">
        <breadcrumbs v-bind:list="{{$breadcrumbs}}"></breadcrumbs>
        <panel title="Editar Matéria" bg="info">
        @php($action =  route('materias') . '/' . $collection->id )
            @if($errors->all())
                @foreach($errors->all() as $key => $val)
                    <div class="col-md-4">
                        <alert message="{{$val}}"></alert>
                    </div>
                @endforeach
            @elseif(session('status'))
                <div class="row">
                    <div class="col-md-12">
                        <alert message="{{ session('status') }}" type="success" icon="check"></alert>
                    </div>
                </div>
            @endif
            <form-head id="editForm" css="" action="{{ $action }}" method="put" enctype="" token="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="subject">Matéria</label>
                            <input type="text" class="form-control" id="subject" name="subject" maxlength="100" value="{{ $collection->subject }}"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="parent_id"> Categoria </label>
                            <select class="form-control" id="parent_id" name="parent_id">
                                <option style="display:none;" value="0">Selecionar ...</option>
                                @foreach( $subjectsParent as $key => $v)
                                    <option value="{{ $v->id }}" {{ $collection->id == $v->id ? 'selected' : '' }}>
                                        {{ $v->subject }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="keywords">Palavras Chaves</label>
                            @php ($keywords = '')
                            @foreach($collection->keywords as $keyword)
                                @php ($keywords .= $keyword->name . ', ')
                            @endforeach
                            @php ($keywords = substr($keywords, 0, -2))
                            <input type="text" class="form-control" id="keywords" name="keywords" placeholder="Separado por vírgulas" value="{{ $keywords }}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="description">Descrição</label>
                    <textarea class="form-control" id="description" name="description" maxlength="300" rows="3">{{ $collection->description }}</textarea>
                </div>
                <div class="form-group">
                    <label for="content">Conteúdo</label>
                    <textarea id="content" name="content">
                        {{ $collection->content }}
                    </textarea>
                </div>
                <button type="submit" class="btn btn-success btn-block col-md-3 float-md-right mx-sm-0 mx-md-2 my-2">
                    Atualizar
                </button>
            </form-head>
                <a href="{{ route('materias') }}">
                    <button class="btn btn-secondary col-md-3 float-md-right my-2">
                        Cancelar
                    </button>
                </a>
        </panel>
    </page>
@endsection

@section('js')
    <script>
        $(function () {
            let editor = CKEDITOR.replace('content', {
                height: 400
            });

            setTimeout(function() {
                $(".alert").fadeOut();
            }, 5000)
        });
    </script>
@stop