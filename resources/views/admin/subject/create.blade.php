@extends('layouts.app')

@section('content')
    <page size="12">
        <breadcrumbs v-bind:list="{{$breadcrumbs}}"></breadcrumbs>
        <panel title="Criar Matéria" bg="info">
            @if($errors->all())
                @foreach($errors->all() as $key => $val)
                    <div class="col-md-4">
                        <alert message="{{$val}}"></alert>
                    </div>
                @endforeach
            @endif
            <form-head id="createForm" css="" action="{{ route('materias.store') }}" method="post" enctype="" token="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="subject">Matéria</label>
                            <input type="text" class="form-control" id="subject" name="subject" maxlength="100" value="{{ old('subject') }}"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="parent_id"> Categoria </label>
                            <select class="form-control" id="parent_id" name="parent_id">
                                <option style="display:none;" value="0">Selecionar ...</option>
                                @foreach( $subjectsParent as $key => $v)
                                    <option value="{{ $v->id }}" {{ old('parent_id') == $v->id ? 'selected' : '' }}>{{ $v->subject }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="keywords">Palavras Chaves</label>
                            <input type="text" class="form-control" id="keywords" name="keywords" placeholder="Separado por vírgulas" value="{{ old('keywords') }}"/>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <label for="description">Descrição</label>
                    <textarea class="form-control" id="description" name="description" maxlength="300" rows="3">{{ old('description') }}</textarea>
                </div>
                <div class="form-group">
                    <label for="content">Conteúdo</label>
                    <textarea id="content" name="content">
                        {{  old('content') }}
                    </textarea>
                </div>

                <button type="submit" class="btn btn-success col-md-3 float-md-right mx-sm-0 mx-md-2 my-2" value="{{ old('data') }}">
                    Salvar
                </button>
            </form-head>
            <a href="{{ route('materias') }}">
                <button class="btn btn-secondary col-md-3 float-md-right my-2">
                    Cancelar
                </button>
            </a>
        </panel>
    </page>
@endsection

@section('js')
    <script>
        $(function () {
            let editor = CKEDITOR.replace('content', {
                height: 400
            });
        });
    </script>
@stop