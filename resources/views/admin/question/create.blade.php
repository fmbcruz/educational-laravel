@extends('layouts.app')

@section('content')
    <page size="12">
        <breadcrumbs v-bind:list="{{$breadcrumbs}}"></breadcrumbs>
        <panel title="Criar Matéria" bg="info">
            @if($errors->all())
                @foreach($errors->all() as $key => $val)
                    <div class="row">
                        <div class="col-md-12">
                            <alert message="{{$val}}"></alert>
                        </div>
                    </div>
                @endforeach
            @endif
            <form-head id="createForm" css="" action="{{ route('questoes.store') }}" method="post" enctype="" token="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="subject_id"> Matéria </label>
                            <select class="form-control" id="subject_id" name="subject_id">
                                <option style="display:none;" value="0">Selecionar ...</option>
                                @foreach( $subjectsParent as $key => $v)
                                    <option value="{{ $v->id }}" {{ old('subject_id') == $v->id ? 'selected' : '' }}>
                                        {{ $v->subject }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="institute">Banca/ Instituição</label>
                            <input type="text" class="form-control" id="institute" name="institute" value="{{ old('institute') }}"/>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="year">Banca/ Instituição</label>
                            <input type="number" min="1990" max="{{ date("Y") }}"  class="form-control" id="year" name="year" value="{{ old('year') }}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="question">Enúnciado da questão</label>
                    <textarea id="content" name="content">
                        {{ old('question') }}
                    </textarea>
                </div>

                <alternatives></alternatives>

                <button type="submit" class="btn btn-success col-md-3 float-md-right mx-sm-0 mx-md-2 my-2" value="{{ old('data') }}">
                    Salvar
                </button>
            </form-head>
            <a href="{{ route('questoes') }}">
                <button class="btn btn-secondary col-md-3 float-md-right my-2">
                    Cancelar
                </button>
            </a>
        </panel>
    </page>
@endsection

@section('js')
    <script>
        $(function () {
            let editor = CKEDITOR.replace('content', {
                height: 400
            });
        });
    </script>
@stop
