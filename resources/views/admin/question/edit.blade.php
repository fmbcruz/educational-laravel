@extends('layouts.app')

@section('content')
    <page size="12">
        <breadcrumbs v-bind:list="{{$breadcrumbs}}"></breadcrumbs>
        <panel title="Criar Matéria" bg="info">
            @if($errors->all())
                @foreach($errors->all() as $key => $val)
                    <div class="row">
                        <div class="col-md-12">
                            <alert message="{{$val}}"></alert>
                        </div>
                    </div>
                @endforeach
            @elseif(session('status'))
                <div class="row">
                    <div class="col-md-12">
                        <alert message="{{ session('status') }}" type="success" icon="check"></alert>
                    </div>
                </div>
            @endif
            @php($action =  route('questoes') . '/' . $collection->id )
            <form-head id="createForm" css="" action="{{ $action }}" method="put" enctype="" token="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="subject_id"> Matéria </label>
                            <select class="form-control" id="subject_id" name="subject_id">
                                <option style="display:none;" value="0">Selecionar ...</option>
                                @foreach( $subjectsParent as $key => $v)
                                    <option value="{{ $v->id }}" {{ $collection->subject_id == $v->id ? 'selected' : '' }}>
                                        {{ $v->subject }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="institute">Banca/ Instituição</label>
                            <input type="text" class="form-control" id="institute" name="institute" value="{{ $collection->institute }}"/>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="year">Ano</label>
                            <input type="number" min="1990" max="{{ date("Y") }}" class="form-control" id="year" name="year" value="{{ $collection->year }}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="question">Enúnciado da questão</label>
                    <textarea id="content" name="content">
                        {{ $collection->question }}
                    </textarea>
                </div>

            <div class="form-group">
                <div class="row">
                    <span class="col-md-6">
                        <label for="alternative">Alternativas <small> (Marque a correta)</small></label>
                    </span>
                </div>
                <span id="editors" class="form-inline form-check-inline">
                @foreach($collection->alternatives as $alternative)
                    <input type="checkbox" id="right_{{$alternative->id}}" name="right[{{ $alternative->id }}]" class="form-control my-2 col-sm-1" {{ $alternative->right != 0 ? 'checked' : '' }}>
                    <input type="text" id="alternative_{{ $alternative->id }}" name="alternatives[{{ $alternative->id }}]" class="form-control my-2 col-sm-11" value="{{ $alternative->alternative }}">
                @endforeach
                </span>
            </div>

                <button type="submit" class="btn btn-success col-md-3 float-md-right mx-sm-0 mx-md-2 my-2" value="{{ old('data') }}">
                    Salvar
                </button>
            </form-head>
            <a href="{{ route('questoes') }}">
                <button class="btn btn-secondary col-md-3 float-md-right my-2">
                    Cancelar
                </button>
            </a>
        </panel>
    </page>
@endsection

@section('js')
    <script>
        $(function () {
            let editor = CKEDITOR.replace('content', {
                height: 400
            });

            setTimeout(function() {
                $(".alert").fadeOut();
            }, 5000)
        });
    </script>
@stop