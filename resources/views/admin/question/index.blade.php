@extends('layouts.app')

@section('content')
    <page size="12">
        @if($errors->all())
            @foreach($errors->all() as $key => $val)
                <div class="col-md-12">
                    <alert message="{{$val}}"></alert>
                </div>
            @endforeach
        @endif
        <breadcrumbs v-bind:list="{{$breadcrumbs}}"></breadcrumbs>
        <panel title="Lista de Questões" bg="info">
            <table-list
                    v-bind:titles="['#', 'Questão', 'Ano', 'Respostas', 'Matéria']"
                    v-bind:items="{{ json_encode($collection) }}"
                    create="{{ route('criar.questoes') }}"
                    edit="{{ route('questoes') }}"
            ></table-list>
            <div class="float-left my-2 pagination total-paginate">
                Total de Registros: <span>{{ count($collection) }}</span>
            </div>
            <div class="float-right my-2">
                {{ $collection->links() }}
            </div>
        </panel>
    </page>
@endsection
