@extends('layouts.app')
<style>
    .luck-numers span {
        font-weight: bolder;
        vertical-align: middle;
        line-height: 1.5rem;
        box-shadow: 2px 2px #0003;
        display: inline-block;
        margin: 12px 12px 0 0;
        font-family: sans-serif;
        font-size: 2rem;
        color: #fff;
        background: #209869;
        border-radius: 35px;
        width: 67px;
        height: 67px;
        padding: 20px 0;
        text-align: center;
    }
    .luck-statistics .title {
        font-size: 1rem;
        text-align: center;
        font-weight: bolder;
        text-transform: capitalize;
        background: #cecece80;
        padding: 10px;
    }
    .luck-statistics p {
         font-size: 0.85rem;
         text-align: center;
     }

</style>
@section('content')
    <page size="12">
        @if($errors->all())
            @foreach($errors->all() as $key => $val)
                <div class="col-md-4">
                    <alert message="{{$val}}"></alert>
                </div>
            @endforeach
        @endif
        <breadcrumbs v-bind:list="{{$breadcrumbs}}"></breadcrumbs>
    </page>
    @if(count($values) > 0)
        <page size="12">
            <div class="row">
                <div class="col-md-12">
                    <panel title="Número da Sorte" bg="info">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 offset-md-3 luck-numers">
                                <span>{{ $values['numbers']['dez_1'] }}</span>
                                <span>{{ $values['numbers']['dez_2'] }}</span>
                                <span>{{ $values['numbers']['dez_3'] }}</span>
                                <span>{{ $values['numbers']['dez_4'] }}</span>
                                <span>{{ $values['numbers']['dez_5'] }}</span>
                                <span>{{ $values['numbers']['dez_6'] }}</span>
                            </div>
                        </div>
                    </panel>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 luck-statistics">
                    <panel title="Estatísticas" bg="danger">
                        <div class="row">
                            @php($names = [
                            'dez_1' => 'Primeira Dezena',
                            'dez_2' => 'Segunda Dezena',
                            'dez_3' => 'Terceira Dezena',
                            'dez_4' => 'Quarta Dezena',
                            'dez_5' => 'Quinta Dezena',
                            'dez_6' => 'Sexta Dezena',
                            ])
                            @foreach($values['dezs'] as $key => $value)
                            <div class="col-md-2">
                                <p class="title">{{ $names[$key] }}</p>
                                <p><b>Média: </b>{{ $value['media'] }}</p>
                                <p><b>Variância: </b>{{ $value['variancia'] }}</p>
                                <p><b>Desvio: </b>{{ $value['desvio_padrao'] }}</p>
                                <p><b>Variou: </b>{{ abs( $value['media'] - $values['numbers'][$key] ) }}</p>
                            </div>
                            @endforeach
                        </div>
                    </panel>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <panel title="Últimos Números" bg="dark">
                        <table-list
                                v-bind:titles="['#', 'Data', '1', '2', '3', '4', '5', '6']"
                                v-bind:items="{{ json_encode($values['old_lottery']) }}"
                        ></table-list>
                        <div class="float-left my-2 pagination total-paginate">
                            Total de Registros: <span>{{ $values['old_lottery']->total() }}</span>
                        </div>
                        <div class="float-right my-2">
                            {{ $values['old_lottery']->links() }}
                        </div>
                    </panel>
                </div>
            </div>
        </page>
    @else
        <page size="12">
            <h3 class="text-center m-5">Não há sorteios.</h3>
        </page>
    @endif

@endsection
