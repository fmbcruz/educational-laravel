@extends('layouts.app')

@section('content')
    <page size="12">
        <breadcrumbs v-bind:list="{{$breadcrumbs}}"></breadcrumbs>
        <panel title="Criar Simulado" bg="info">
            @if($errors->all())
                @foreach($errors->all() as $key => $val)
                    <div class="row">
                        <div class="col-md-12">
                            <alert message="{{$val}}"></alert>
                        </div>
                    </div>
                @endforeach
            @endif
            <form-head id="createForm" css="" action="{{ route('simulados.store') }}" method="post" enctype="" token="{{ csrf_token() }}">
                <div class="row">
                    <input hidden name="user_id" value="{{ Auth::user()->id }}">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name">Defina um nome para o simulado</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}"/>
                        </div>
                    </div>
                    <div class="col-md-2 offset-md-2">
                        Quantidade
                    </div>
                    <div class="col-md-6">
                        Matéria
                    </div>
                    @php($n = 4)
                    @while($n > 0)
                        <div class="col-md-2 offset-md-2">
                            <div class="form-group">
                                <input type="text" class="form-control" name="qtd[{{ $n }}]">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <select class="form-control" id="subject_id" name="subject_id[{{ $n }}]">
                                <option style="display:none;" value="0">Selecionar ...</option>
                                @foreach( $subjectsParent as $key => $v)
                                    <option value="{{ $v->id }}">
                                        {{ $v->subject }}
                                    </option>
                                @endforeach
                            </select>
                            @php($n--)
                        </div><br>
                    @endwhile
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-success col-md-3 float-md-right mx-md-2 my-4" value="{{ old('data') }}">
                            Criar
                        </button>
                        <a href="{{ route('simulados') }}">
                            <button type="button" class="btn btn-secondary col-md-3 float-md-right my-4">
                                Cancelar
                            </button>
                        </a>
                    </div>
                </div>
            </form-head>
        </panel>
    </page>
@endsection
