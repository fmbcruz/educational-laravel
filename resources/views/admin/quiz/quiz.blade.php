@extends('layouts.app')

<style>
    .box-result {
        position: fixed;
        border: 1px solid;
        padding: 14px 26px 0px;
        top: 40%;
        right: 8.85%;
        box-shadow: 2px 4px 1px #88888842;
        z-index: -1;
    }

    @media (min-width: 1250px) and  (max-width: 1500px) {
        .box-result {
            right: 1%;
        }
    }
    @media (max-width: 1250px) {
        .box-result {
            position: relative;
            border: 1px solid;
            display: inline-flex;
            padding: 15px 26px 0px;
            top: 0;
            right: 0;
            box-shadow: 2px 4px 1px #88888842;
            z-index: 1;
        }

        .box-result p:first-child {
            padding-right: 25px;
        }

        .box-result p:last-child {
            padding-left: 20px;
        }
    }

    .note-alert {
        position: relative;
        -webkit-animation: note-alert 1s infinite alternate; /* Safari 4.0 - 8.0 */
        animation: note-alert 1s infinite alternate;
    }

    /* Safari 4.0 - 8.0 */
    @-webkit-keyframes note-alert {
        from {transform: scale(0.8);}
        to {transform: scale(1.5);}
    }

    @keyframes note-alert {
        from {transform: scale(0.7);}
        to {transform: scale(1.5);}
    }

    hr {
        margin-top: 2rem;
        margin-bottom: 2rem;
        border: 0;
        border-top: 2px dashed rgba(0, 0, 0, 0.27);
    }

    .custom-control.custom-radio {
        border-radius: 6px;
        padding-left: 28px;
    }

</style>

@section('content')
    <page size="12">
        @if($errors->all())
            @foreach($errors->all() as $key => $val)
                <div class="row">
                    <div class="col-md-12">
                        <alert message="{{$val}}"></alert>
                    </div>
                </div>
            @endforeach
        @elseif(session('status'))
            <div class="row">
                <div class="col-md-12">
                    <alert message="{{ session('status') }}" type="success" icon="check"></alert>
                </div>
            </div>
        @endif
        @php(array_push($breadcrumbs, ['title' => ucfirst($collection->name) . ' - ' . ucfirst(Request::route('subject')), 'url' => '']))
        <breadcrumbs v-bind:list="{{json_encode($breadcrumbs)}}"></breadcrumbs>

        @php ($subject_id = Request::route('subject_id') )

            <nav class="nav nav-pills nav-fill my-3">
                <a class="nav-item nav-link {{ $subject_id == 4 ? 'active' : '' }}"
                   href="{{ route('fazer.simulados', [$collection->id, 'portugues', 4]) }}">Português</a>
                <a class="nav-item nav-link {{ $subject_id == 3 ? 'active' : '' }}"
                   href="{{ route('fazer.simulados', [$collection->id, 'historia', 3]) }}">História</a>
                <a class="nav-item nav-link {{ $subject_id == 2 ? 'active' : '' }}"
                   href="{{ route('fazer.simulados', [$collection->id, 'geografia', 2]) }}">Geografia</a>
                <a class="nav-item nav-link {{ $subject_id == 1 ? 'active' : '' }}"
                   href="{{ route('fazer.simulados', [$collection->id, 'informatica', 1]) }}">Informática</a>
            </nav>

        <panel title="{{ $collection->name }}" bg="info">
            @php($action =  route('simulados') . '/' . $collection->id )
            <form-head id="createForm" css="" action="{{ $action }}" method="put" enctype="" token="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-md-12">
                        @php ($countErros = 0)
                        @php ($countRights = 0)
                        @php ($total = 0)
                        @foreach( $collection->questions as $question )
                            @php(isset($links[$subject_id]) ? $links[$subject_id] : $links[$subject_id] = [])
                            @if( $question->subject_id == $subject_id || in_array($question->subject_id, $links[$subject_id]) )
                                @php ($total++)
                                <p class="text-justify"><b>({{ $question->institute }} - {{ $question->year }})</b> {!! $question->question !!} </p>
                                <div class="form-group">
                                    @foreach( $question->alternatives as $alternative )
                                        @php ($right = false)
                                        @php ($answerID = null)
                                        @php ($answered = null)
                                        @foreach( $answers as $key => $answer )
                                            @if( $answer->alternative_id && $question->id == $answer->question_id )
                                                @php ($answered = true)
                                                @if( $question->id == $answer->question_id && $alternative->right !== 0 && $alternative->id == $answer->alternative_id )
                                                    @php($right = true)
                                                    @php ($countRights++)
                                                @endif
                                            @if( $alternative->right !== 1 && $alternative->id == $answer->alternative_id)
                                                    @php($answerID = $answer->alternative_id)
                                                    @php($countErros++)
                                                @endif
                                            @endif
                                        @endforeach
                                            <div class="custom-control custom-radio col-md-12 my-2 {{ ($alternative->right && $answered) ? 'text-success font-weight-bold' : '' }}{{ (!$alternative->right && $answerID) ? 'text-danger font-weight-bold' : '' }}">

                                                <input type="checkbox"
                                                       id="question[{{ $question->id }}][{{ $alternative->id }}]"
                                                       name="question[{{ $question->id }}][{{ $alternative->id }}]"
                                                       class="custom-control-input" {{ (($alternative->right && $answered) && ($alternative->right && $right)) ? 'checked' : '' }}{{ (!$alternative->right && $answerID) ? 'checked' : '' }}>
                                                <label class="custom-control-label" for="question[{{ $question->id }}][{{ $alternative->id }}]">
                                                    {{ $alternative->alternative }}
                                                </label>
                                            </div>
                                    @endforeach
                                </div>
                                <hr>
                            @endif
                        @endforeach

                        @if($countRights != 0 || $countErros != 0)
                            @php( $noteTotalRight =  number_format(($countRights * 100 / ($countRights + $countErros)), 1, '.', ','))
                            <div class="box-result bg-dark text-white">
                                <p data-toggle="tooltip" data-placement="top" title="{{ $noteTotalRight }}"><i class="fas fa-check-circle text-success"></i> {{ $countRights }}</p>
                                <p data-toggle="tooltip" data-placement="top" title="{{ (100 - $noteTotalRight) }}"><i class="fas fa-times-circle text-danger {{ $noteTotalRight <= 70 ? 'note-alert' : ''}}"></i> {{ $countErros }}</p>
                                <p><i class="fas fa-edit text-{{ (($countRights + $countErros) == $total) ? 'secondary' : 'warning' }}"></i>
                                    {{ $countRights + $countErros }} / {{ $total }}
                                </p>
                            </div>
                        @endif

                        <button type="submit" class="btn btn-success col-md-3 float-md-right mx-sm-0 mx-md-2 my-2" value="{{ old('data') }}">
                            Salvar
                        </button>
                        <a href="{{ route('simulados') }}">
                            <button type="button" class="btn btn-secondary col-md-3 float-md-right my-2">
                                Sair
                            </button>
                        </a>
                    </div>
                </div>
            </form-head>
        </panel>
    </page>
@endsection

@section('js')
    <script>
        $(function () {
            setTimeout(function() {
                $(".alert").fadeOut();
            }, 5000)
        });
    </script>
@stop