@extends('layouts.app')

<style>
    .progress {
        height: 1.5rem !important;
        font-size: 0.8rem !important;
    }
</style>

@section('content')
    <page size="12">
        @if($errors->all())
            @foreach($errors->all() as $key => $val)
                <div class="col-md-4">
                    <alert message="{{$val}}"></alert>
                </div>
            @endforeach
        @endif
            <breadcrumbs v-bind:list="{{$breadcrumbs}}"></breadcrumbs>
    </page>
    @if(count($statistics) > 0)
    <page size="12">
        <div class="row">
            <div class="col-md-12">
                <panel title="Gráficos" bg="info">
                    <div class="row">
                        <div class="col-md-8">
                            {!! $line->render() !!}
                        </div>
                        <div class="col-md-4">
                            {!! $pie->render() !!}
                        </div>
                    </div>
                </panel>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <panel title="Detalhes" bg="info">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Matéria</th>
                            <th scope="col">Acertos</th>
                            <th scope="col">Erros</th>
                            <th scope="col">Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $totalQuestoes = 0;
                            $totalAcertos = 0;
                            $totalErros = 0;
                        @endphp
                        @foreach($statistics as $key => $statistic)
                            @php
                                $totalQuestoes += $statistic->total;
                                $totalAcertos += $statistic->acertos;
                                $totalErros += $statistic->erros;
                            @endphp
                        <tr>
                            <th scope="row">{{ ++$key }}</th>
                            <td>{{ $statistic->label }}</td>
                            <td>{{ $statistic->acertos }}</td>
                            <td>{{ $statistic->erros }}</td>
                            <td>{{ $statistic->total }}</td>
                        </tr>
                        @endforeach
                        <tfoot>
                        <tr>
                            <td colspan="2" class="text-center"><b>TOTAIS</b></td>
                            <td>{{ $totalAcertos }}</td>
                            <td>{{ $totalErros }}</td>
                            <td>{{ $totalQuestoes }}</td>
                        </tr>
                        </tfoot>
                    </table>
                </panel>
            </div>
        </div>
    </page>
    @else
    <page size="12">
        <h3 class="text-center m-5"> Não há resolução de simulados.</h3>
    </page>
    @endif

@endsection
