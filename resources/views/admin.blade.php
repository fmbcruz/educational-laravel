@extends('layouts.app')

@section('content')
    <page size="12">
        <panel title="Dashboard" bg="info">
            <div class="row">
                <div class="col-md-3" >
                    <box qtd="{{ $totalResumes }}" title="Matérias" bg="#00c0ef" icon="far fa-newspaper" url="{{ route('materias') }}"></box>
                </div>
                <div class="col-md-3" >
                    <box qtd="{{ $totalQuestions }}" title="Questões" bg="#dd4b39" icon="far fa-question-circle" url="{{ route('questoes') }}"></box>
                </div>
                <div class="col-md-3" >
                    <box qtd="{{ $totalQuizzes }}" title="Simulado" bg="#0cb356" icon="fas fa-pencil-alt" url="{{ route('simulados') }}"></box>
                </div>
                <div class="col-md-3" >
                    <box qtd="&infin;" title="Estatísticas" bg="#f1c114" icon="fas fa-chart-pie" url="{{ route('estatisticas') }}"></box>
                </div>
            </div>
        </panel>
    </page>
    <div class="mt-5"></div>
    <page size="12">
        <panel title="Mega Sena" bg="dark">
            <div class="row">
                <div class="col-md-3">
                    <box qtd="Mega Sena" title="Sorteios" bg="rgb(76, 27, 116)" icon="far fa-money-bill-alt" url="#nenhum"></box>
                </div>
            </div>
        </panel>
    </page>
@endsection