@extends('layouts.app')

<style>
    @import url('https://fonts.googleapis.com/css?family=Montserrat:400,600,700');
    table {
        background: #e5e5e5;
    }
    img {
        padding: 0.25rem !important;
        background-color: #f5f8fa !important;
        border: 1px solid #dee2e6 !important;
        border-radius: 0.25rem !important;
    }
    body {
        color: #383838 !important;
        font-family: 'Montserrat', sans-serif !important;
    }
    p.small.mt-3 a {
        background-color: #17a2b8;
        padding: 0.2rem;
        color: #fff;
        border-radius: 5px;
    }
    h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6{
        margin-bottom: 1.5rem !important;
    }
    h2, .h2 {
        font-weight: 700;
    }
    div#app {
        background: #f5f8fa;
    }
    .card-header.bg-info.text-white {
        padding: 2rem;
    }
    pre {
        background: #e5e5e5;
        padding: 1.5rem;
    }
    th, td {
        padding: 1.5rem 3rem;
        text-align: left;
        border: 1px solid rgba(0, 0, 0, 0.1);
    }
    .card-body {
        padding: 3rem 2rem;
    }
</style>

@section('content')
    <page size="12">
        <div class="card">
            <div class="card-header bg-info text-white">
                <h2>{{ $subject->subject }}</h2>
                <p class="lead">{{ $subject->description }}</p>
            </div>
        </div>
        <p class="small mt-3"><i class="fa fa-tags" aria-hidden="true"></i>
            @php( $n = 0)
            @foreach($subject->keywords as $j => $keyword)
                <a href="{{ route('keywords', $keyword->id) . '/' .  str_slug($keyword->name) }}">{{ ucfirst($keyword->name) }}</a>
            @endforeach
        </p>
        <div class="card">
            <div class="card-body">
                {!! $subject->content !!}
            </div>
        </div>
    </page>
@endsection
