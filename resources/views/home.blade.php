@extends('layouts.app')

@section('content')

    <page size="12">

        @if($errors->all())
            @foreach($errors->all() as $key => $val)
                <alert message="{{$val}}"></alert>
            @endforeach
        @endif

            <div class="row">
                <form class="col-md-4 form-group" action="{{ route('home') }}" method="get" >
                    <div class="input-group">
                        <input type="search" class="form-control border-right-0 border" name="busca" placeholder="Buscar resumo " value="{{ isset($search) ? $search : '' }}">
                        <span class="input-group-append">
                                    <button class="input-group-text bg-white" type="submit">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                </span>
                    </div>
                </form>
            </div>

            @php($totalTag = count($collection))
            @if(!empty($search) && $total > 1)
                <p> {{ $total }} resultados encontrados para <b>{{ $search }}</b> </p>
            @elseif(!empty($search) && $total == 1)
                <p> {{ $total }} resultado encontrado para <b>{{ $search }}</b> </p>
            @elseif(!empty($search) && $total <= 0)
                <p> Não foram encontrados registros com <b>{{ $search }}</b> </p>
            @elseif(\Request::route()->getName() == 'keywords' && $totalTag > 1)
                <p> {{ count($collection) }} resultados para a tag <b>{{ Request::route('keyword') }}</b> </p>
            @elseif(\Request::route()->getName() == 'keywords' && $totalTag < 1)
                <p> {{ count($collection) }} resultado para a tag <b>{{ Request::route('keyword') }}</b> </p>
            @endif

            @if( !empty($collection) )
            <div class="card-columns">
                @php ($i = 0)
                @foreach( $collection as $key => $v)
                    @php ($urlView = route('ver.materias', $v->id) . '/' . str_slug($v->subject))
                    @if (Auth::check())
                        @php ($urlEdit = route('editar.materias', $v->id))
                    @else
                        @php ($urlEdit = null)
                    @endif
                    <card-text
                        size="10"
                        title="{{ $v->subject }}"
                        description="{{ str_limit($v->description, 150, ' ...') }}"
                        url="{{ $urlView }}"
                        edit="{{ $urlEdit }}"
                    >
                        @php( $n = 0)
                        @foreach($v->keywords as $j => $keyword)
                            <a href="{{ route('keywords', $keyword->id) . '/' .  str_slug($keyword->name) }}">{{ ucfirst($keyword->name) }}</a>{{ (count($v->keywords) - ++$n) == 0 ? '' : ', ' }}
                        @endforeach
                    </card-text>
                @endforeach
            </div>
            <div class="float-right my-2">
                <!-- Método que monta a paginação -->
                {{ $collection->links() }}
            </div>
            @elseif(empty($search) && empty($collection))
                <p class="text-dark">Não há registros para mostrar</p>
            @endif
    </page>

@endsection