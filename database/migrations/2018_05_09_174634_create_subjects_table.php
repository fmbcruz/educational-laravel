<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject', 100);
            $table->unsignedInteger('parent_id')->nullable();
            $table->string('description', 300)->nullable();
            $table->longText('content')->nullable();
            $table->smallInteger('children_count')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('parent_id')->references('id')->on('subjects');
        });

        $subjects = ['Informática', 'Geografia', 'História', 'Português', 'Inglês'];

        foreach ($subjects as $key => $subject)
        {
            DB::table('subjects')->insert(
                array(
                    ['subject' => $subject],

                )
            );

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subjects');
    }
}
