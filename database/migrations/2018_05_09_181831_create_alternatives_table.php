<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlternativesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alternatives', function (Blueprint $table) {
            $table->increments('id');
            $table->text('alternative')->nullable(false);
            $table->unsignedInteger('question_id');
            $table->boolean('right')->default(false);
            $table->foreign('question_id')->references('id')->on('questions');
        });

//        $n = 1;
//        while($n <= 50){
//            DB::table('alternatives')->insert(['alternative' => 'favoreceu a ampliação das trocas comerciais com povos andinos, que dominavam as técnicas de produção de cerâmica e as transmitiram aos povos guarani.', 'question_id' => $n, 'right' => 1]);
//            DB::table('alternatives')->insert(['alternative' => 'possibilitou que os povos que a praticavam se tornassem sedentários e pudessem armazenar alimentos, criando a necessidade de fabricação de recipientes para guardá-los.', 'question_id' => $n, 'right' => 0]);
//            DB::table('alternatives')->insert(['alternative' => 'proliferou, sobretudo, entre os povos dos sambaquis, que conciliaram a produção de objetos de cerâmica com a utilização de conchas e ossos na elaboração de armas e ferramentas.', 'question_id' => $n, 'right' => 0]);
//            DB::table('alternatives')->insert(['alternative' => 'difundiu-se, originalmente, na ilha de Fernando de Noronha, região de caça e coleta restritas, o que forçava as populações locais a desenvolver o cultivo de alimentos.', 'question_id' => $n, 'right' => 0]);
//            $n++;
//        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alternatives');
    }
}
