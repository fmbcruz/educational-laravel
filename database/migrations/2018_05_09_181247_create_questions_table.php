<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->text('question')->nullable(false);
            $table->string('institute', 50)->nullable();
            $table->smallInteger('year')->nullable();
            $table->unsignedInteger('subject_id')->nullable();
            $table->smallInteger('question_count')->default(0);
            $table->timestamps();
            $table->foreign('subject_id')->references('id')->on('subjects');
        });

        Schema::create('quiz_question', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('alternative_id')->nullable();
            $table->unsignedInteger('question_id');
            $table->unsignedInteger('quiz_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('question_id')->references('id')->on('questions');
            $table->foreign('quiz_id')->references('id')->on('quizzes');
        });


//        $n = 1;
//        while ($n <= 50){
//            $subjects = [1=>'Informática', 'Geografia', 'História', 'Português', 'Inglês'];
//            $subject = rand(1,5);
//            DB::table('questions')->insert([
//                        'question' => $subjects[$subject] . ' - Considerando o <b>modelo</b> proposto pelo PMI (Project Management Institute) para o gerenciamento de projetos, analise as proposições abaixo e, em seguida, assinale a alternativa correta que aponta as fases de um projeto na qual estão inseridos os processos relacionados à área de conhecimento de Gerenciamento de Escopo.',
//                        'institute'=>'CESPE',
//                        'year' => 2018,
//                        'subject_id' => $subject,
//                    ]
//            );
//            $n++;
//        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
