<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Question extends Model
{

    protected $fillable = [
        'id', 'question', 'institute', 'year', 'subject_id', 'question_count'
    ];

    public static function getAllQuestions($page_size)
    {
        return DB::table('questions as QST')
                ->join('subjects as SUB', 'QST.subject_id', '=', 'SUB.id')
                ->select('QST.id',
                    'QST.question',
                    'QST.year',
                    'QST.question_count',
                    'SUB.subject')
                ->orderBy('id')
                ->paginate($page_size);
    }

    public static function setAlternatives($questionId, $alternatives=[], $right=[], $update)
    {
        $alternatives = array_filter($alternatives);

        if($update) {
            $alternativesId = [];
            foreach ($alternatives as $key => $alternative) {
                array_push($alternativesId, $key);
            }

            Alternative::destroy($alternativesId);
        }

        foreach ($alternatives as $key => $alternative) {

            if(!empty($right[$key]))
                $isRight = 1;
            else
                $isRight = 0;

            DB::table('alternatives')
                ->insert(['alternative' => $alternative, 'question_id' => $questionId, 'right' => $isRight]);
        }
    }

    public function alternatives()
    {
        return $this->hasMany('App\Alternative');
    }

    public function Quizzes()
    {
        return $this->belongsToMany('App\Quiz');
    }

    public static function search($term)
    {
        return DB::table('questions as QST')
            ->join('alternatives as ALT', 'QST.id', '=', 'ALT.question_id')
            ->join('subjects as SUB', 'QST.subject_id', '=', 'SUB.id')
            ->where('QST.question', 'like', "%". strtolower($term) . "%")
            ->orWhere('ALT.alternative', 'like', "%". strtolower($term) . "%")
            ->orWhere('SUB.subject', 'like', "%". strtolower($term) . "%")
            ->select('QST.id', 'QST.question', 'QST.year', 'QST.question_count', 'SUB.subject')
            ->distinct()
            ->paginate(25);
    }
}
