<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Alternative extends Model
{

    protected $fillable = [
        'id', 'alternative', 'institute', 'year', 'question_id', 'right'
    ];

    public $timestamps = false;
}
