<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Quiz extends Model
{

    protected $fillable = [
        'id', 'name', 'total_questions', 'total_rights', 'total_lefts', 'total_answers'
    ];

    protected $casts = [
        'created_at' => 'datetime:d/m/Y',
        'updated_at' => 'datetime:d/m/Y',
    ];

    public static function generateQuiz($quiz_id, $data=[])
    {
        foreach($data['subject_id'] as $n => $subject){

            $questions = DB::table('questions as QST')
                ->select('QST.id')
                ->join('subjects as SUB', 'QST.subject_id','=', 'SUB.id')
                ->orWhere('QST.subject_id','=', $subject)
                ->orWhere('SUB.parent_id', '=', $subject)
                ->limit($data['qtd'][$n])
                ->inRandomOrder()->get();

            $answers = [];
            foreach ($questions as $key => $question) {
                array_push($answers,[
                    'user_id' => $data['user_id'],
                    'question_id' => $question->id,
                    'quiz_id' => $quiz_id,
                ]);
                DB::table('quizzes')->whereId($quiz_id)->increment('total_questions');
                DB::table('questions')->whereId($question->id)->increment('question_count');
            }

            DB::table('quiz_question')->insert($answers);
        }
    }

    public static function setQuiz($data = [], int $idQuiz)
    {
        foreach ($data['question'] as $idQuestion => $question) {

            $idAlternative = array_keys($question);

            DB::table('quiz_question')
                ->where('quiz_id', '=', $idQuiz)
                ->where('question_id', '=', $idQuestion)
                ->update(['alternative_id' => $idAlternative[0]]);
        }
    }

    public static function getAnswers($id, $idSubject)
    {
        return DB::table('quiz_question as ANS')
            ->join('questions as QST', 'ANS.question_id', '=', 'QST.id')
            ->select('ANS.*')
            ->where('ANS.quiz_id', '=', $id)
            ->where('QST.subject_id', '=', $idSubject)
            ->get();
    }

    public static function getSubjectsQuestion($id)
    {
        $resp = [];

        $array = DB::table('quiz_question as QQ')
            ->join('questions as QS', 'QQ.question_id', '=', 'QS.id')
            ->leftJoin('subjects as SB', 'QS.subject_id', '=', 'SB.id')
            ->leftJoin('subjects as SB2', 'QS.subject_id', '=', 'SB2.parent_id')
            ->select('SB.id', 'SB.parent_id')
            ->where('QQ.quiz_id', '=', $id)
            ->groupBy('QS.subject_id')
            ->get();

        foreach ($array as $item) {
            if(!empty($item->parent_id)) {
                $resp[$item->parent_id] = [];
                if(isset($item->parent_id)) {
                    array_push($resp[$item->parent_id], $item->id);
                }
            }
        }

        return $resp;
    }

    public static function getSubjects($id)
    {
        return DB::table('subject')
            ->select('SB.subject', 'SB.id')
            ->where('QQ.quiz_id', '=', $id)
            ->groupBy('QS.subject_id')
            ->get();
    }

    public function questions()
    {
        return $this->belongsToMany('App\Question', 'quiz_question');
    }

    public static function setRights($quizId)
    {
        $rights =  Quiz::countAlternatives($quizId, 1);
        $errors = Quiz::countAlternatives($quizId, 0);
        $total  = $rights + $errors;

        Quiz::find($quizId)->update(['total_rights' => $rights, 'total_lefts' => $errors, 'total_answers' => $total]);

    }

    private static function countAlternatives($quizId, $opt)
    {
        return DB::table('quiz_question as ANS')
            ->join('quizzes as QUI', 'ANS.quiz_id', '=', 'QUI.id')
            ->leftJoin('alternatives as ALT', 'ANS.alternative_id', '=', 'ALT.id')
            ->where('QUI.id',$quizId)
            ->where('ALT.right', $opt)
            ->count();
    }

    public static function getStatistics($rights = true)
    {
        $totalRight = DB::table('quiz_question as ANSs')
            ->join('questions as QST', 'ANS.question_id', '=', 'QST.id')
            ->join('alternatives as ALT', 'ANS.alternative_id', '=', 'ALT.id')
            ->join('subjects as REL', 'QST.subject_id', '=', 'REL.id')
            ->where('ALT.right', '=',$rights)
            ->where('REL.parent_id', '=', null)
            ->select('REL.subject', DB::raw('count(*) as total'))
            ->groupBy('REL.subject_id')
            ->get();

        return $totalRight;
    }

    public static function search($term)
    {
        return DB::table('quizzes')
            ->where('name', 'like', '%'. strtolower($term) . '%')
            ->paginate(25);
    }

}
