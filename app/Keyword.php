<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keyword extends Model
{
    protected $fillable = [
        'id', 'name'
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'pivot'
    ];

    public function subjects()
    {
        return $this->belongsToMany('App\Subject');
    }

}
