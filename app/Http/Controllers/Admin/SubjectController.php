<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Subject;
use Validator;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $breadcrumbs = json_encode([
            ['title' => 'Dashboard', 'url' => route('admin')],
            ['title' => 'Matérias', 'url' => ''],
        ]);

        $search = $request->busca;

        if(empty($search)) {
            $collection = Subject::getAllSubjects(5);
            $subjectsParent = Subject::getSubjectsParent();
        } else {
            $collection = Subject::search($search);
        }

        return view('admin.subject.index', compact('collection', 'subjectsParent', 'breadcrumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = json_encode([
            ['title' => 'Dashboard', 'url' => route('admin')],
            ['title' => 'Matérias', 'url' => route('materias')],
            ['title' => 'Criar Matéria', 'url' => ''],
        ]);
        $subjectsParent = Subject::getSubjectsParent();
        return view('admin.subject.create', compact('subjectsParent', 'breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = $this->validateData($data);

        if($validator->fails())
            return redirect()->back()->withErrors($validator)->withInput();

        $subject = Subject::create($data);

        Subject::setKeywords($subject->id, $data['keywords']);

        return redirect()->route('editar.materias', ['id' => $subject->id])->with('status', 'Matéria inserida com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $breadcrumbs = json_encode([
            ['title' => 'Dashboard', 'url' => route('admin')],
            ['title' => 'Matérias', 'url' => route('materias')],
            ['title' => 'Editar Matéria', 'url' => ''],
        ]);
        $collection = Subject::find($id);
        $subjectsParent = Subject::getSubjectsParent();
        return view('admin.subject.edit', compact('collection', 'subjectsParent', 'breadcrumbs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        $validator = $this->validateData($data);

        if($validator->fails())
            return redirect()->back()->withErrors($validator)->withInput();

        Subject::find($id)->update($data);

        Subject::setKeywords($id, $data['keywords']);

        return redirect()->back()->with('status', 'Matéria atualizada com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Subject::find($id)->delete();

        return redirect()->back();
    }

    protected function validateData($data)
    {
        $rules = [
            'subject'   => 'required|max:100',
            'description' => 'max:300',
        ];

        $messages = [
            'subject.required'    => 'Informe o nome da matéria.',
            'subject.max'         => 'O nome da matéria deve conter no máximo 100 caracteres.',
            'subject.description' => 'A descrição da matéria deve conter no máximo 300 caracteres.',
        ];

        return Validator::make($data, $rules, $messages);

    }
}
