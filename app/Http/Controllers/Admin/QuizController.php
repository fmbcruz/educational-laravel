<?php

namespace App\Http\Controllers\Admin;

use App\Subject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Quiz;
use Validator;

class QuizController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $breadcrumbs = json_encode([
            ['title' => 'Dashboard', 'url' => route('admin')],
            ['title' => 'Simulados', 'url' => ''],
        ]);

        $search = $request->busca;

        if(empty($search))
            $collection = Quiz::select('*')->paginate(10);
        else
            $collection = Quiz::search($search);

        if(!empty($collection) && empty($search)){
            $date = null;
            $time = new \DateTime();
            foreach ($collection as $key => $quiz) {

                $diff = $time->diff($quiz->updated_at);
                $diff = $diff->i;

                if($diff < 5)
                    $date = true;
                Quiz::setRights($quiz->id);
            }

            if($date)
                $collection = Quiz::select('*')->paginate(10);
        }


        return view('admin.quiz.index', compact('collection', 'subjectsParent', 'breadcrumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = json_encode([
            ['title' => 'Dashboard', 'url' => route('admin')],
            ['title' => 'Simulados', 'url' => route('simulados')],
            ['title' => 'Criar Simulado', 'url' => ''],
        ]);

        $subjectsParent = Subject::getSubjectsMain();

        return view('admin.quiz.create', compact( 'breadcrumbs', 'subjectsParent'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'name' => 'required',
        ]);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $quiz = Quiz::create($data);

        Quiz::generateQuiz($quiz->id, $data);

        $params = [
            'id'         => $quiz->id,
            'subject'    => 'portugues',
            'subject_id' => 4
        ];

        return redirect()->route('fazer.simulados', $params)->with('status', 'Simulado gerado com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $subject, $subject_id)
    {
        $breadcrumbs = [
            ['title' => 'Dashboard', 'url' => route('admin')],
            ['title' => 'Simulados', 'url' => route('simulados')],
        ];

        $collection = Quiz::find($id);
        $links = Quiz::getSubjectsQuestion($id, $subject_id);
        $answers = Quiz::getAnswers($id, $subject_id);

        foreach ($links as $link){
            $array = Quiz::getAnswers($id, $link);
            $answers = $answers->merge($array);
        }

        foreach ($collection->questions as $key => $question) {
            if (isset($links[$subject_id]) && $question->subject_id != $subject_id) {
                if(!in_array($question->subject_id, (array)$links[$subject_id])) {
                    unset($collection->questions[$key]);
                }
            }
        }

//        return $collection;

        return view('admin.quiz.quiz', compact('collection', 'answers', 'breadcrumbs', 'links'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'question' => 'required',
        ]);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        Quiz::setQuiz($data, $id);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
