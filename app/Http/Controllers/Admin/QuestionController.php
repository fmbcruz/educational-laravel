<?php

namespace App\Http\Controllers\Admin;

use App\Question;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Subject;
use Validator;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $breadcrumbs = json_encode([
            ['title' => 'Dashboard', 'url' => route('admin')],
            ['title' => 'Questões', 'url' => ''],
        ]);

        $search = $request->busca;

        if(empty($search))
            $collection = Question::getAllQuestions(10);
        else
            $collection = Question::search($search);

        return view('admin.question.index', compact('collection', 'breadcrumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = json_encode([
            ['title' => 'Dashboard', 'url' => route('admin')],
            ['title' => 'Questões', 'url' => route('questoes')],
            ['title' => 'Criar Questão', 'url' => ''],
        ]);

        $subjectsParent = Subject::getSubjectsParent();

        return view('admin.question.create', compact('subjectsParent', 'breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $data['question'] = $data['content'];

        $validator = $this->validateData($data);

        if($validator->fails())
            return redirect()->back()->withErrors($validator)->withInput();

        $question = Question::create($data);

        isset($data['right']) ? $data['right'] : $data['right'] = [];

        Question::setAlternatives($question->id, $data['alternatives'], $data['right'], false);

        return redirect()->route('editar.questoes', ['id' => $question->id])->with('status', 'Questão inserida com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $breadcrumbs = json_encode([
            ['title' => 'Dashboard', 'url' => route('admin')],
            ['title' => 'Questões', 'url' => route('questoes')],
            ['title' => 'Editar Questão', 'url' => ''],
        ]);
        $collection = Question::find($id);
        $subjectsParent = Subject::getSubjectsParent();

        return view('admin.question.edit', compact('collection', 'subjectsParent', 'breadcrumbs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        $data['question'] = !empty($data['content']) ? $data['content'] : null;

        $validator = $this->validateData($data);

        if($validator->fails())
            return redirect()->back()->withErrors($validator)->withInput();

        Question::find($id)->update($data);

        isset($data['right']) ? $data['right'] : $data['right'] = [];

        Question::setAlternatives($id, $data['alternatives'], $data['right'], true);

        return redirect()->back()->with('status', 'Questão atualizada com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $questions = Question::find($id);
        $questions->alternatives()->delete();
        $questions->delete();

        return redirect()->back();
    }

    protected function validateData($data)
    {
        $rules = [
            'subject_id' => 'required|not_in:0',
            'question' => 'required',
            'year' => 'nullable|min:4|max:4',
            'alternatives' => 'required',
        ];

        $messages = [
            'subject_id.required'  => 'Selecione a matéria',
            'subject_id.not_in'    => 'Selecione a matéria',
            'question.required'    => 'Defina um enunciado para a questão',
            'year.min'             => 'O ano não pode ter menos de 4 caracteres',
            'year.max'             => 'O ano não pode ter mais de 4 caracteres',
            'alternatives.required'=> 'Informe alterantivas para a questão'
        ];

        return Validator::make($data, $rules, $messages);

    }
}
