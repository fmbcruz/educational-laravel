<?php

namespace App\Http\Controllers\Admin;

use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\LuckNumber;
use App\Lottery;

class LotteryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $breadcrumbs = json_encode([
            ['title' => 'Dashboard', 'url' => route('admin')],
            ['title' => 'Mega Sena', 'url' => ''],
        ]);

        $collection = Lottery::all();
        $lottery = [];

        foreach ($collection as $key => $item) {

            $lottery[$key]['numbers'] = [ $item->dezena_1, $item->dezena_2, $item->dezena_3, $item->dezena_4, $item->dezena_5, $item->dezena_6];
            sort($lottery[$key]['numbers']);

            $lottery['dez_1'][] = $lottery[$key]['numbers'][0];
            $lottery['dez_2'][] = $lottery[$key]['numbers'][1];
            $lottery['dez_3'][] = $lottery[$key]['numbers'][2];
            $lottery['dez_4'][] = $lottery[$key]['numbers'][3];
            $lottery['dez_5'][] = $lottery[$key]['numbers'][4];
            $lottery['dez_6'][] = $lottery[$key]['numbers'][5];

            $lottery[$key]['sum'] = $item->dezena_1 + $item->dezena_2 + $item->dezena_3 + $item->dezena_4 + $item->dezena_5 + $item->dezena_6;
            $lottery['sum'][]  = $lottery[$key]['sum'];


        }

        $lottery['sum'] = array_count_values($lottery['sum']);

        $dez['dez_1'] = array_count_values($lottery['dez_1']);
        $dez['dez_2'] = array_count_values($lottery['dez_2']);
        $dez['dez_3'] = array_count_values($lottery['dez_3']);
        $dez['dez_4'] = array_count_values($lottery['dez_4']);
        $dez['dez_5'] = array_count_values($lottery['dez_5']);
        $dez['dez_6'] = array_count_values($lottery['dez_6']);


        $values['dezs']['dez_1'] =  $this->getStatistics($dez['dez_1']);
        $values['dezs']['dez_2'] =  $this->getStatistics($dez['dez_2']);
        $values['dezs']['dez_3'] =  $this->getStatistics($dez['dez_3']);
        $values['dezs']['dez_4'] =  $this->getStatistics($dez['dez_4']);
        $values['dezs']['dez_5'] =  $this->getStatistics($dez['dez_5']);
        $values['dezs']['dez_6'] =  $this->getStatistics($dez['dez_6']);
        $values['soma']  =  $this->getStatistics($lottery['sum']);

        $values['numbers']      = $this->getLuck($values);
        $values['old_lottery']  = LuckNumber::getOldNumbers();

        return view('admin.lottery.index', compact('values', 'breadcrumbs'));
    }

    public function getLuck($statistics = []){

        $numbers = [];
        $limit_low  = $statistics['soma']['media'] - $statistics['soma']['desvio_padrao'] - 45;
        $limit_high = $statistics['soma']['media'] - $statistics['soma']['desvio_padrao'] + 40;
        $n = 1;

        foreach ($statistics['dezs'] as $statistic) {
            $init = (int)($statistic['media'] - $statistic['variancia'] - 1);
            $numbers[('dez_' . $n)] = rand($init, $statistic['media']);
            $n++;
        }
        $numbers['sum'] = array_sum($numbers);

        if( $numbers['sum'] >= $limit_low &&
            $numbers['sum'] <= $limit_high &&
            (count($numbers) == count(array_unique($numbers)))) {
            LuckNumber::create($numbers);
            return $numbers;
        } else {
            return false;
        }
    }

    public function getStatistics($value=[]){

        // Calcula media ponderada
        $freq = 0;
        $num  = 0;
        foreach ($value as $key => $item) {
            $freq = $freq + $item;
            $num = $num + ($key * $item);
        }

        $avg = $num/$freq;

        // Calculo de variância e desvio padrão
        $pow = 0;
        foreach ($value as $key => $item) {
            $sum = $key - $avg;
            $pow = $pow + ($sum * $sum);
        }
        $values['media'] = number_format($avg, 2, '.', '');
        $values['variancia'] = number_format(($pow/$freq), 2, '.', '');
        $values['desvio_padrao'] = number_format(sqrt($values['variancia']), 2, '.', '');
        $values['numeros'] = $value;

        return $values;

    }
}
