<?php

namespace App\Http\Controllers\Admin;

use App\Alternative;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Subject;
use App\Question;
use App\Quiz;

class StatisticsController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumbs = json_encode([
            ['title' => 'Dashboard', 'url' => route('admin')],
            ['title' => 'Estatísticas', 'url' => ''],
        ]);

        $statistics = DB::table('quiz_question as QQ')
            ->select('SUB.subject as label',
                DB::raw('sum(CASE WHEN ALT.right = 1 THEN 1 ELSE 0 END) as acertos'),
                DB::raw('sum(CASE WHEN ALT.right = 0 THEN 1 ELSE 0 END) as erros'),
                DB::raw('count(*) as total'))
            ->join('alternatives as ALT', 'QQ.alternative_id', '=', 'ALT.id')
            ->join('questions as QST', 'ALT.question_id', '=', 'QST.id')
            ->join('subjects as SUB', 'QST.subject_id', '=', 'SUB.id')
            ->where('QQ.alternative_id', '<>', null)
            ->groupBy('SUB.subject')
            ->orderBy('total', 'desc')
            ->get();

        $data['label'] = [];
        $data['acertos'] = [];
        $data['erros'] = [];
        foreach ($statistics as $statistic) {
            array_push($data['label'], $statistic->label);
            array_push($data['acertos'], $statistic->acertos);
            array_push($data['erros'], $statistic->erros);
        }

        $line = $this->getChart('line',$data, 'Acertos', 'Erros');

        $data = $this->formatData($statistics);

        $pie = $this->getChart('pie',$data, 'Quantidade de exercícios', '', 'pie');

        return view('admin.statistic.index', compact('statistics','breadcrumbs', 'line', 'pie'));
    }

    public function getChart($chart='doughnut', $data=[], $title1=null, $title2=null,$options=null)
    {

        $titleDisplay =  $title1 ? 'true' : 'false';

        if($chart === 'line'):
            $dataset = [[
                "label" => $title1,
                'backgroundColor' => "rgba(34, 206, 206, 0.31)",
                'borderColor' => "rgba(34, 206, 206, 0.7)",
                "pointBorderColor" => "rgba(34, 206, 206, 0.7)",
                "pointBackgroundColor" => "rgba(34, 206, 206, 0.7)",
                "pointHoverBackgroundColor" => "rgba(34, 206, 206, 0.7)",
                "pointHoverBorderColor" => "rgba(220,220,220,1)",
                'data' => $data['acertos'],
            ],
            [
                "label" => $title2,
                'backgroundColor' => "rgba(255, 99, 132, 0.31)",
                'borderColor' => "rgba(255, 99, 132, 0.7)",
                "pointBorderColor" => "rgba(255, 99, 132, 0.7)",
                "pointBackgroundColor" => "rgba(255, 99, 132, 0.7)",
                "pointHoverBackgroundColor" => "rgba(255, 99, 132, 0.7)",
                "pointHoverBorderColor" => "rgba(220,220,220,1)",
                'data' => $data['erros'],
            ]];
        else:
            $dataset = [[
                'backgroundColor' => $data['bg'],
                'hoverBackgroundColor' => $data['bgHover'],
                'data' => $data['data']
            ]];

        endif;

        $chart = app()->chartjs
            ->name($chart)
            ->type($chart)
            ->size(['width' => 500, 'height' => 500])
            ->labels($data['label'])
            ->datasets($dataset);
        if(empty($options)):
            return $chart->options([]);
        elseif($options == 'bar'):
            return $chart->optionsRaw("{
            responsive: true,
            legend: {
                display:false
            },
            scales: {
                xAxes: [{
                    gridLines: {
                        display:false
                    }  
                }]
            },
            tooltips: {
              displayColors: false
            },
            title: {
                display: " . $titleDisplay . ",
                text: '" . $title1 . "'
            }
        }");
        elseif($options == 'pie'):
            return $chart->optionsRaw("{
            responsive: true,
            legend: {
                display:false
            },
            tooltips: {
              displayColors: false
            },
            title: {
                display: " . $titleDisplay . ",
                text: '" . $title1 . "'
            }
        }");
        endif;
    }

    public function formatData($totals)
    {
        $data['label'] = [];
        $data['data'] = [];
        $data['bg'] = [];
        $data['bgHover'] = [];

        foreach ($totals as $key => $value) {
            $color = $this->generateColor($key);
            array_push($data['label'] ,$value->label);
            array_push($data['bg'] ,$color);
            array_push($data['bgHover'] ,$color . 'b8');
            array_push($data['data'] ,$value->total);
        }

        return $data;
    }

    public function generateColor(&$n)
    {
        $colors = ['ff6384', 'ff9f40', '96f', '36a2eb', 'ffcd56', '4bc0c0', 'c9cbcf'];
        if($n >= count($colors))$n = 0;
        return '#'.$colors[$n];
    }
}
