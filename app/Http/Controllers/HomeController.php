<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Subject;
use App\Question;
use App\Quiz;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $totalResumes   = Subject::where('deleted_at', null)->whereNotNull('content')->count();
        $totalQuestions = Question::whereNotNull('id')->count();
        $totalQuizzes   = Quiz::whereNotNull('id')->count();

        return view('admin', compact('totalResumes', 'totalQuestions', 'totalQuizzes'));
    }
}
