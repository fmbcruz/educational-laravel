<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Lottery extends Model
{

    protected $table = "lottery";

    protected $fillable = [
        'concurso', 'data_sorteio', 'dezena_1', 'dezena_2', 'dezena_3', 'dezena_4', 'dezena_5', 'dezena_6'
    ];

    public function alternatives()
    {
        return $this->hasMany('App\Alternative');
    }

    public function Quizzes()
    {
        return $this->belongsToMany('App\Quiz');
    }

}
