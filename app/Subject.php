<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Subject extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id', 'subject', 'description', 'content', 'parent_id', 'children_count'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    protected $dates = ['deleted_at'];

    public static function getAllSubjects ($page_size)
    {
        return Subject::select('id', 'subject', 'description')
            ->whereNotNull('content')
            ->paginate($page_size);
    }

    public static function getAllSubjectsForKeywords ($page_size, $keyword_id)
    {
        return Subject::select('id', 'subject', 'description')
            ->whereNotNull('content')
            ->whereHas('keywords', function ($query) use ($keyword_id) {
                $query->where('id', $keyword_id);
            })
            ->paginate($page_size);
    }

    public static function searchHome($term)
    {
        return Subject::select('id', 'subject', 'description')
            ->whereNotNull('content')
            ->where('subject', 'like', "%". strtolower($term) . "%")
            ->orWhere('description', 'like', "%". strtolower($term) . "%")
            ->orWhere('content', 'like', "%". strtolower($term) . "%")
            ->paginate(25);
    }

    public static function getSubjectsParent()
    {
        return Subject::all(['id', 'subject'])->sortBy('subject');
    }

    public static function getSubjectsMain()
    {
        return Subject::select('id', 'subject')
            ->where('parent_id', '=', null)
            ->orderBy('subject')
            ->get();
    }

    public static function setKeywords($idSubject, $keywords)
    {
        $keywords = mb_strtolower($keywords);
        $keywords = explode(',', $keywords);
        $keywords = array_filter(array_map('trim', $keywords));
        $keywordsId = [];

        foreach ($keywords as $key => $val) {

            $rsp = DB::table('keywords')->select('id')->where('name', '=', $val)->first();

            if(empty($rsp->id)) {
                $id = DB::table('keywords')->insertGetId(['name' => $val]);
                array_push($keywordsId, $id);
            } else {
                array_push($keywordsId, $rsp->id);
            }
        }

        $subject = Subject::find($idSubject);
        $subject->keywords()->sync($keywordsId);

    }

    public function keywords()
    {
        return $this->belongsToMany('App\Keyword', 'subject_keyword');
    }

    public static function search($term)
    {
        return DB::table('subjects')
            ->where('subject', 'like', "%". strtolower($term) . "%")
            ->orWhere('description', 'like', "%". strtolower($term) . "%")
            ->orWhere('content', 'like', "%". strtolower($term) . "%")
            ->select('id', 'subject', 'description')
            ->distinct()
            ->paginate(25);
    }
}
