<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class LuckNumber extends Model
{

    protected $table = "luck_numers";

    protected $fillable = [
        'id', 'date', 'dez_1', 'dez_2', 'dez_3', 'dez_4', 'dez_5', 'dez_6'
    ];

    public $timestamps = false;

    public static function getOldNumbers(){
        return DB::table('luck_numers')->orderBy('date', 'desc')->paginate(10);
    }

}
