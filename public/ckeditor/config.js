/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {

    // Config toolbar
    config.toolbar = [{
        name: 'styles',
        items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat', 'Blockquote', '-', 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'Link', 'Unlink', '-', 'Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar'],
    },
        {
            name: 'tools',
            items: ['Styles', 'Format', 'Font', 'FontSize', '-', 'TextColor', 'BGColor', '-', 'Find', 'Replace', 'SelectAll', '-', 'Undo', 'Redo', '-', 'Maximize', 'ShowBlocks', 'Source']
        }];

    // Upload files
    config.filebrowserImageBrowseUrl = '/uploads?type=Images';
    config.filebrowserImageUploadUrl = '/uploads/upload?type=Images&_token=';
    config.filebrowserBrowseUrl = '/uploads?type=Files';
    config.filebrowserUploadUrl = '/uploads/upload?type=Files&_token=';

};
