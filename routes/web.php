<?php

use App\Subject;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (Request $request) {

    $search = isset($request->busca) ? $request->busca : '' ;

    if(isset($search) && !empty($search)) {
        $collection = Subject::searchHome($search);
        $total = count($collection);
    } else {
        $collection = Subject::getAllSubjects(12);
    }


    return view('home', compact('collection', 'search', 'total'));
})->name('home');

Auth::routes();

// Redirect route register
Route::match(['get', 'post'], 'register', function(){
    return redirect('/');
});

Route::get('/admin', 'HomeController@index')->name('admin');

// Mostra conteúdo do resumo
Route::get('/materia/{id}/{title?}', function ($id){
    $subject = Subject::find($id);
    if($subject) {
        return view('subject', compact('subject'));
    }
})->name('ver.materias');

// Mostra os resumos com a keyword informada
Route::get('/palavra-chave/{id}/{keyword?}', function ($id){
    $collection = Subject::getAllSubjectsForKeywords(12, $id);
    return view('home', compact('collection'));
})->name('keywords');

Route::middleware('auth')->prefix('admin')->namespace('Admin')->group(function(){
    // Matérias
    Route::resource('materias', 'SubjectController');
    Route::get('/materias', 'SubjectController@index')->name('materias');
    Route::get('/materia/criar', 'SubjectController@create')->name('criar.materias');
    Route::get('/materia/{id}/editar', 'SubjectController@show')->name('editar.materias');

    // Questões
    Route::resource('questoes', 'QuestionController');
    Route::get('/questoes', 'QuestionController@index')->name('questoes');
    Route::get('/questao/criar', 'QuestionController@create')->name('criar.questoes');
    Route::get('/questao/{id}/editar', 'QuestionController@show')->name('editar.questoes');

    // Simulado
    Route::resource('simulados', 'QuizController');
    Route::get('/simulados', 'QuizController@index')->name('simulados');
    Route::get('/simulado/criar', 'QuizController@create')->name('criar.simulados');
    Route::get('/simulado/{id}/{subject}/{subject_id}', 'QuizController@show')->name('fazer.simulados');

    // Estatísticas
    Route::get('/estatisticas', 'StatisticsController@index')->name('estatisticas');

    // Lottery
    Route::get('/mega', 'LotteryController@index')->name('mega');
});